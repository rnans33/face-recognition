from torch.utils import data
from torchvision import transforms
from PIL import Image
import os
#import sys
#sys.path.insert(0, 'C:/Users/rnans/PycharmProjects/Deep-face-recognition/mtcnn_pytorch_master')
class VGGFace2(data.Dataset):

    def __init__(self, number=None, sampling=None,split='train'):
        self.path = "/home/jju/pycharm/dataset/vggface2/"
        self.split = split
        self.number = number
        self.sampleing = sampling
        #RGB_MEAN = [0.485, 0.456, 0.406]
        #RGB_STD = [0.229, 0.224, 0.225]

        RGB_MEAN = [0.5, 0.5, 0.5]
        RGB_STD = [0.5, 0.5, 0.5]

        self.label_id = {}
        self.img_info = []

        if self.split == 'train':
            self.path_split = self.path + "train/"
            self.file = "train_list.txt"
        else:
            self.path_split = self.path + "test/"
            self.file = "test_list.txt"

        class_id = None
        count = 0
        sample = 1
        with open(self.path + self.file) as f:
            for i, img_file in enumerate(f):

                if i % 100000 == 0:
                    print("processing: {} imges for {}".format(i, self.split))

                if number is not None and len(self.label_id)  == self.number + 1:
                    self.img_info.pop()
                    self.label_id.pop(class_id)
                    break

                img_file = img_file.strip()  # n004332/0317_01.jpg
                class_id = img_file.split("/")[0]  # n004332

                if class_id not in self.label_id:
                    self.label_id[class_id] = count
                    count = count + 1
                    sample = 0

                sample = sample + 1

                if self.sampleing is not None and sample >= self.sampleing:
                    continue

                self.img_info.append({
                    'cid' : class_id,
                    'img' : img_file,
                    'label' : self.label_id[class_id]
                })


        if self.split == 'train':
            self.transform = transforms.Compose([
                transforms.Resize((128,128)),
                #transforms.RandomCrop(128),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize(mean=RGB_MEAN,
                                     std=RGB_STD),
            ])
        else:
            self.transform = transforms.Compose([
                transforms.Resize((128,128)),
                #transforms.CenterCrop(128),
                transforms.ToTensor(),
                transforms.Normalize(mean=RGB_MEAN,
                                     std=RGB_STD),
            ])

    def __getitem__(self, index):
        info = self.img_info[index]
        img_file = info['img']
        label_ = info['label']

        img_path = os.path.join(self.path_split, img_file)
        img = Image.open(img_path)
        img = self.transform(img)

        return img, label_

    def __len__(self):
        return len(self.img_info)

if __name__ == "__main__":
    dataset = VGGFace2(number=None)

    a = dataset.__len__()
    print(a)
    count = 0
    for i in range(85, 87):
        img, la = dataset.__getitem__(i)
    print(img.shape)
    print(len(dataset.label_id))
