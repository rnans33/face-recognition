from torch.utils import data
import torch
import numpy as np
import matplotlib.pyplot as plt
from torch.utils import data
from torchvision import transforms
from PIL import Image

import net

class LFW(data.Dataset):

    def __init__(self):
        self.path = "./lfw-align-128.tar/lfw-align-128/"


        RGB_MEAN = (0.5, 0.5, 0.5)
        RGB_STD = (0.5, 0.5, 0.5)

        self.label_id = {}
        self.img_info = []

        self.file = "./lfw_test_pair.txt"

        with open(self.file) as f:
            for i, img_file in enumerate(f):

                img_ = img_file.split()

                self.img_info.append({
                    'pair1': self.path + img_[0],
                    'pair2': self.path + img_[1],
                    'label': img_[2]
                })

                if i % 1000 == 0:
                    print("processing: {} imges".format(i))


        self.transform = transforms.Compose([
            transforms.Resize((112,96)),
            transforms.ToTensor(),
            transforms.Normalize(mean=RGB_MEAN,
                                 std=RGB_STD),
        ])

    def __getitem__(self, index):
        info = self.img_info[index]
        pair1 = info['pair1']
        pair2 = info['pair2']
        label_ = info['label']

        pair1 = Image.open(pair1)
        pair2 = Image.open(pair2)

        pair1 = self.transform(pair1)
        pair2 = self.transform(pair2)

        return pair1, pair2, label_

    def __len__(self):
        return len(self.img_info)

    def pair_info(self):
        return self.img_info

def cosin_metric(x1, x2):
    return np.dot(x1, x2) / (np.linalg.norm(x1) * np.linalg.norm(x2))

def cal_accuracy(y_score, y_true):
    y_score = np.asarray(y_score)
    y_true = np.asarray(y_true)
    best_acc = 0
    best_th = 0
    for i in range(len(y_score)):
        th = y_score[i]
        y_test = (y_score >= th)
        acc = np.mean((y_test == y_true).astype(int))
        if acc > best_acc:
            best_acc = acc
            best_th = th

    return best_acc, best_th

def imsavefig(img, str_):
    img = img / 2 + 0.5     # unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.savefig(str_)

if __name__ == "__main__":

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)

    model = net.sphere()
    model.to(device)
    model.load_state_dict(torch.load("./ACC99.28.pth"))

    model.eval()
    test_batch_size = 1

    vaildset = LFW()
    vaildloader = torch.utils.data.DataLoader(vaildset, batch_size=test_batch_size,
                                              shuffle=False, num_workers=4)

    print('{} vaild total:'.format(len(vaildloader)))

    sims = []
    labels = []

    for ii, data in enumerate(vaildloader):
        pair_1, pair_2, label = data

        pair_1 = pair_1.to(device)
        pair_2 = pair_2.to(device)

        # print(type(label), type(label[0]), label[0])

        pair_1 = model(pair_1)
        pair_2 = model(pair_2)

        pair_1 = pair_1.data.cpu().numpy()
        pair_2 = pair_2.data.cpu().numpy()

        # print(pair_1.shape, pair_2.shape)
        label = int(label[0])

        sim = cosin_metric(pair_1, pair_2.T)

        sims.append(sim)
        labels.append(label)

        #print(sim, label)
        if ii + 1 % 1000 == 0:
            print(ii + "iter")

    acc, th = cal_accuracy(sims, labels)

    print('lfw face verification accuracy: ', acc, 'threshold: ', th)

